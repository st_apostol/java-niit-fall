package com.niit.zoo;

import com.niit.zoo.animals.Animal;

/**
 * Created by Сергей on 25.09.2016.
 */
public class Cage {

    private int capacity;
    private Animal animal_in_cage;

    public Cage(int capacity) {
        this.capacity = capacity;
    }

    public boolean putAnimal(Animal animal) {
        if (this.capacity >= animal.getWeight()) {
            this.animal_in_cage = animal;
            return true;
        }
        else { return false; }
    }

    public Animal whoLivesHere() { return this.animal_in_cage; }

    public boolean releaseInhabitant() {
        if (this.animal_in_cage.getWeight() > this.capacity) {
            return false;
        }
        return true;
    }

}