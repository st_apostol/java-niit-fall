package com.niit.zoo.food;

/**
 * Created by Сергей on 29.09.2016.
 */
public abstract class Food {
    protected int nutricity;
    protected boolean fresh = true;

    public Food(int nutricity) {
        if (nutricity < 0) {
            this.nutricity = 0;
        }
        else {
            this.nutricity = nutricity;
        }
    }

    public int getNutricity() {
        return this.nutricity;
    }

    public boolean getFresh() { return this.fresh; }

    public void setFresh(boolean fresh) {
        this.fresh = fresh;
    }
}
