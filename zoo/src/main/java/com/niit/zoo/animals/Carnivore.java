package com.niit.zoo.animals;

import com.niit.zoo.food.Food;
import com.niit.zoo.food.Meat;
import com.niit.zoo.food.Vegetable;

/**
 * Created by Сергей on 25.09.2016.
 */
public class Carnivore extends Animal {
    public Carnivore(String n, int w) {
        super(n, w);
    }

    @Override
    public void eat(Food food) {
        if (food instanceof Meat) {
            if (food.getFresh()) {
                this.weight += food.getNutricity();
            }
            else {
                this.weight -= food.getNutricity();
            }
        }
        else {
            this.weight -= food.getNutricity();
        }
    }
}
