package com.niit.zoo.animals;

/**
 * Created by Сергей on 25.09.2016.
 */
public class Horse extends Herbivore {
    public Horse(String name, int weight) {
        super(name, weight);
        this.soundFile = "horse.wav";
    }
}
