package com.niit.zoo.animals;

/**
 * Created by Сергей on 25.09.2016.
 */
public class Hyena extends Omnivore {
    public Hyena(String name, int weight) {
        super(name, weight);
        this.soundFile = "hyena.wav";
    }
}
