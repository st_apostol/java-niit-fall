package com.niit.zoo.animals;

/**
 * Created by Сергей on 25.09.2016.
 */
public class Lion extends Carnivore {
    public Lion(String name, int weight) {
        super(name, weight);
        this.soundFile = "lion.wav";
    }
}
