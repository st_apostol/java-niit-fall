package com.niit.zoo.animals;

import com.niit.zoo.food.Food;
import com.niit.zoo.Player;

/**
 * Created by Сергей on 25.09.2016.
 */
public abstract class Animal {

    private String name;
    protected int weight;
    protected String soundFile;

    public Animal(String name, int weight) {
        this.name = name;
        this.weight = weight;
    }

    public String getName() { return name; }

    public double getWeight() { return weight; }

    public String getSoundFile() { return soundFile; }

    public void makeSound() { Player.play(soundFile); }

    public abstract void eat(Food food);
}