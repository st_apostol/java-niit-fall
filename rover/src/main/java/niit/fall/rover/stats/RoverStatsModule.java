package niit.fall.rover.stats;

import niit.fall.rover.Point;

import java.util.Collection;

public interface RoverStatsModule {
    void registerPosition(Point position);

    boolean isVisited(Point point);

    Collection<Point> getVisitedPoints();
}
