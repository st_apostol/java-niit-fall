package niit.fall.rover;

import niit.fall.rover.constants.Direction;

public interface Turnable {
    void turnTo(Direction direction);
}
