package niit.fall.rover;

import niit.fall.rover.constants.Direction;

public interface Landable {
    void land(Point position, Direction direction);
}
