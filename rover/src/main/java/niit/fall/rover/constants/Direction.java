package niit.fall.rover.constants;

public enum Direction {
    NORTH,
    EAST,
    SOUTH,
    WEST
}
