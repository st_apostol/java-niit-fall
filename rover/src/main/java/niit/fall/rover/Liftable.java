package niit.fall.rover;

public interface Liftable {
    void lift();
}
